/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;

/**
 *
 * @author jhan
 */
public class Servicio {
    
     private int id;
    private String nombre;
    private String descripcion;
    private int precio;

    public Servicio() {
    }

    public Servicio(int id, String nombre, String descripcion, int precio) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Servicio{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio=" + precio + '}';
    }       
    
    
    //Buscar por ID
    
    public Servicio buscarPorID(int id){
        try {
                ArrayList<Servicio> lista = new ArrayList<>();
                Servicio servicio = new Servicio();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_servicio_adm.sp_find_servicio_id(?,?) }");
        cs.setInt(1, id);
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Servicio
                servicio.setId(rs.getInt(1));
                servicio.setNombre(rs.getString(2));
                servicio.setDescripcion(rs.getString(3));
                servicio.setPrecio(rs.getInt(4));
                

            
    } // Cierre ciclo while
           
            return servicio;
            
            } catch (SQLException ex) {
            Logger.getLogger(Servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }; 
    
    //Listar todos los Servicios
    
    public ArrayList<Servicio> listarServicio(){
         try {
                ArrayList<Servicio> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_servicio_adm.sp_list_servicios(?) }");
        cs.registerOutParameter(1, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(1);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                Servicio objServicio = new Servicio();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                objServicio.setId(rs.getInt(1));
                objServicio.setNombre(rs.getString(2));
                objServicio.setDescripcion(rs.getString(3));
                objServicio.setPrecio(rs.getInt(4));
                                
                lista.add(objServicio); //se carga el objeto seteado en el ciclo
            
    } // Cierre ciclo while
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(Servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };
    
    //Crear nuevo servicio
    
    public int crearServicio(Servicio servicio){
        Conector conector   = new Conector();
        Connection con      = conector.connectar();
        
        int cod_error       = 1122;//Codigo ERROR: error ejecucion del procedimeinto almacenado
        
        //extraccion de datos del objeto enviado
        String vnombre       = servicio.getNombre();
        String vdescripcion          = servicio.getDescripcion();
        int vprecio        = servicio.getPrecio();
      

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_servicio_adm.sp_new_servicio(?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setString(2, vnombre);
            cs.setString(3, vdescripcion);
            cs.setInt   (4, vprecio);
           
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
        
    };
    
    //Editar servicio
    
    public int editarServicio(int id, Servicio servicio){
        Conector conector = new Conector();
        Connection con       = conector.connectar();
        int cod_error        = 1122; //error en procedimiento
        String vnombre       = servicio.getNombre();
        String vdescripcion        = servicio.getDescripcion();
        int vprecio       = servicio.getPrecio();
        

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_servicio_adm.sp_edit_servicio(?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, id);
            cs.setString(3, vnombre);
            cs.setString(4, vdescripcion);
            cs.setInt   (5, vprecio);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
           
    
    };
    
    
    
    //Eliminar Servicio
    
    public int eliminarServicio(int id){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_servicio_adm.sp_del_servicio(?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }
    
}
