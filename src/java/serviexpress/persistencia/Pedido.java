/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;


public class Pedido {
    
    private int id;
    private String fecha;
    private int idProv;
    private int idEmp;

    public Pedido(int id, String fecha, int idProv, int idEmp) {
        this.id = id;
        this.fecha = fecha;
        this.idProv = idProv;
        this.idEmp = idEmp;
    }

    public Pedido() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdProv() {
        return idProv;
    }

    public void setIdProv(int idProv) {
        this.idProv = idProv;
    }

    public int getIdEmp() {
        return idEmp;
    }

    public void setIdEmp(int idEmp) {
        this.idEmp = idEmp;
    }

    @Override
    public String toString() {
        return "Pedido{" + "id=" + id + ", fecha=" + fecha + ", idProv=" + idProv + ", idEmp=" + idEmp + '}';
    }
    
    public ArrayList<Pedido> listarOrdenesPedidos(){
         try {
                ArrayList<Pedido> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_orden_pedido_adm.sp_list_ordenes(?) }");
        cs.registerOutParameter(1, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(1);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                Pedido pedido = new Pedido();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                
                pedido.setId(rs.getInt(1));
                pedido.setFecha(rs.getString(2));                
                pedido.setIdProv(rs.getInt(3));
                pedido.setIdEmp(rs.getInt(4));             
                                   
            
                lista.add(pedido);
    } // Cierre ciclo while
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(Pedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };
     
     
     
    // crear cliente
         public int crearOrdenPedido(Pedido pedido){
        Conector conector   = new Conector();
        Connection con      = conector.connectar();
        
        int cod_error       = 1122;//Codigo ERROR: error ejecucion del procedimiento almacenado
        
        //extraccion de datos del objeto enviado        
        int vIdProovedor                   = pedido.getIdProv();
        int vIdEmp                     = pedido.getIdEmp();

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_orden_pedido_adm.sp_new_orden(?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vIdProovedor);
            cs.setInt(3, vIdEmp);
                              
            cs.executeQuery();
            cod_error = cs.getInt(1);           
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Pedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
        
    };
     
         
    public Pedido buscarPorID(int id){
        try {
                ArrayList<Pedido> lista = new ArrayList<>();
                Pedido pedido = new Pedido();
                Conector conector = new Conector();
                Connection con = conector.connectar();        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_orden_pedido_adm.sp_find_orden_id(?,?) }");
        cs.setInt(1, id);      
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
        
            
            while(rs.next()){ // se ejecuta un ciclo que recorre la variable ResultSet

                // Seteo de atributos del Objeto Cliente
                pedido.setId(rs.getInt(1));
                pedido.setFecha(rs.getString(2));                
                pedido.setIdProv(rs.getInt(3));
                pedido.setIdEmp(rs.getInt(4));
                                                          
            
    } // Cierre ciclo while
           
            return pedido;
            
            } catch (SQLException ex) {
            Logger.getLogger(Pedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };
    
    public int eliminarOrdenPedido(int id){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_orden_pedido_adm.sp_del_orden(?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Pedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }
    
    
}
