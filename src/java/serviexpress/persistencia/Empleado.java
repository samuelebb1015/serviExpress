
package serviexpress.persistencia;

//Importaciones de clases y librerias
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;

//declaracion de variables
public class Empleado {
private int             id;
private String          rut;
private String          primerNombre;
private String          segundoNombre;
private String          apellidoMaterno;
private String          apellidoPaterno;
private int             telefono;
private String          direccion;
private String          email;
private String          username;
private String          password;
private int        sucursal;
private int         estado;
private int    tipo;

//Constructor sin parametros
    public Empleado() {
    }

    public Empleado(int id, String rut, String primerNombre, String segundoNombre, String apellidoMaterno, String apellidoPaterno, int telefono, String direccion, String email, String username, String password, int sucursal, int estado, int tipo) {
        this.id = id;
        this.rut = rut;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.apellidoMaterno = apellidoMaterno;
        this.apellidoPaterno = apellidoPaterno;
        this.telefono = telefono;
        this.direccion = direccion;
        this.email = email;
        this.username = username;
        this.password = password;
        this.sucursal = sucursal;
        this.estado = estado;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", rut=" + rut + ", primerNombre=" + primerNombre + ", segundoNombre=" + segundoNombre + ", apellidoMaterno=" + apellidoMaterno + ", apellidoPaterno=" + apellidoPaterno + ", telefono=" + telefono + ", direccion=" + direccion + ", email=" + email + ", username=" + username + ", password=" + password + ", sucursal=" + sucursal + ", estado=" + estado + ", tipo=" + tipo + '}';
    }    
    
    public Empleado loginEmpleado(String username, String password){
        try {
                ArrayList<Empleado> lista = new ArrayList<>();
                Empleado empleado = new Empleado();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_login_empleado(?,?,?) }");
        cs.setString(1, username);
        cs.setString(2, password);
        cs.registerOutParameter(3, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(3);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Condominio
                empleado.setId(rs.getInt(1));
                empleado.setRut(rs.getString(2));
                empleado.setPrimerNombre(rs.getString(3));
                empleado.setSegundoNombre(rs.getString(4));
                empleado.setApellidoMaterno(rs.getString(5));
                empleado.setApellidoPaterno(rs.getString(6));
                empleado.setTelefono(rs.getInt(7));
                empleado.setDireccion(rs.getString(8));
                empleado.setEmail(rs.getString(9));
                empleado.setUsername(rs.getString(10));
                empleado.setPassword(rs.getString(11));
                empleado.setEstado(rs.getInt(13));
                empleado.setSucursal(rs.getInt(14));
                empleado.setTipo(rs.getInt(15));
                
                
    } // Cierre ciclo while
           
            return empleado;
            
            } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };
    
    public Empleado buscarPorID(int id, int idSuc){
        try {
                ArrayList<Empleado> lista = new ArrayList<>();
                Empleado objEmpleado = new Empleado();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_find_empleado_id(?,?,?) }");
        cs.setInt(1, idSuc);
        cs.setInt(2, id);
        cs.registerOutParameter(3, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(3);
        
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Condominio
                objEmpleado.setId(rs.getInt(1));
                objEmpleado.setRut(rs.getString(2));                
                objEmpleado.setPrimerNombre(rs.getString(3));
                objEmpleado.setSegundoNombre(rs.getString(4));
                objEmpleado.setApellidoPaterno(rs.getString(5));
                objEmpleado.setApellidoMaterno(rs.getString(6));
                objEmpleado.setTelefono(rs.getInt(7));
                objEmpleado.setDireccion(rs.getString(8));
                objEmpleado.setEmail(rs.getString(9));                               
                objEmpleado.setSucursal(rs.getInt(10));
                objEmpleado.setUsername(rs.getString(11));
                objEmpleado.setEstado(rs.getInt(12));               
                objEmpleado.setTipo(rs.getInt(13));        
                
                
            
    } // Cierre ciclo while
           
            return objEmpleado;
            
            } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };
    
    
    //buscar por RUT
    
    public Empleado buscarPorRut(String rut, int idSuc){
        try {
                ArrayList<Empleado> lista = new ArrayList<>();
                Empleado objEmpleado = new Empleado();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_find_empleado_rut(?,?,?) }");
        cs.setInt(1, idSuc);
        cs.setString(2, rut);
        cs.registerOutParameter(3, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(3);
        
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Condominio
                objEmpleado.setId(rs.getInt(1));
                objEmpleado.setRut(rs.getString(2));                
                objEmpleado.setPrimerNombre(rs.getString(3));
                objEmpleado.setSegundoNombre(rs.getString(4));
                objEmpleado.setApellidoPaterno(rs.getString(5));
                objEmpleado.setApellidoMaterno(rs.getString(6));
                objEmpleado.setTelefono(rs.getInt(7));
                objEmpleado.setDireccion(rs.getString(8));
                objEmpleado.setEmail(rs.getString(9));                               
                objEmpleado.setSucursal(rs.getInt(10));
                objEmpleado.setUsername(rs.getString(11));
                objEmpleado.setEstado(rs.getInt(12));               
                objEmpleado.setTipo(rs.getInt(13));        
                
                     
                              
            
    } // Cierre ciclo while
           
            return objEmpleado;
            
            } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };    
    
    // listar todos
    
    public ArrayList<Empleado> listarEmpleados(int idSuc){
         try {
                ArrayList<Empleado> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();
                
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_list_empleados(?,?) }");
        cs.setInt(1, idSuc);
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                Empleado objEmpleado = new Empleado();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                
                objEmpleado.setId(rs.getInt(1));
                objEmpleado.setRut(rs.getString(2));                
                objEmpleado.setPrimerNombre(rs.getString(3));
                objEmpleado.setSegundoNombre(rs.getString(4));
                objEmpleado.setApellidoPaterno(rs.getString(5));
                objEmpleado.setApellidoMaterno(rs.getString(6));
                objEmpleado.setTelefono(rs.getInt(7));
                objEmpleado.setDireccion(rs.getString(8));
                objEmpleado.setEmail(rs.getString(9));                               
                objEmpleado.setSucursal(rs.getInt(10));
                objEmpleado.setUsername(rs.getString(11));
                objEmpleado.setEstado(rs.getInt(12));               
                objEmpleado.setTipo(rs.getInt(13));        
                
                lista.add(objEmpleado); //se carga el objeto seteado en el ciclo
            
    } // Cierre ciclo while
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };
    
    
    
    //Listar empleados activos
    public ArrayList<Empleado> listarEmpleadosActivos(int idSuc){
         try {
                ArrayList<Empleado> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();
                
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_list_empleados(?,?) }");
        cs.setInt(1, idSuc);
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                Empleado objEmpleado = new Empleado();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                
                objEmpleado.setId(rs.getInt(1));
                objEmpleado.setRut(rs.getString(2));                
                objEmpleado.setPrimerNombre(rs.getString(3));
                objEmpleado.setSegundoNombre(rs.getString(4));
                objEmpleado.setApellidoPaterno(rs.getString(5));
                objEmpleado.setApellidoMaterno(rs.getString(6));
                objEmpleado.setTelefono(rs.getInt(7));
                objEmpleado.setDireccion(rs.getString(8));
                objEmpleado.setEmail(rs.getString(9));                               
                objEmpleado.setSucursal(rs.getInt(10));
                objEmpleado.setUsername(rs.getString(11));
                objEmpleado.setEstado(rs.getInt(12));               
                objEmpleado.setTipo(rs.getInt(13));      
                                
                lista.add(objEmpleado); //se carga el objeto seteado en el ciclo
            
    } // Cierre ciclo while
            
            
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };
    
    // crear Empleado
    
    public int crearEmpleado(Empleado empleado){
        Conector conector   = new Conector();
        Connection con      = conector.connectar();
        
        int cod_error       = 1122;//Codigo ERROR: error ejecucion del procedimeinto almacenado
        
        //extraccion de datos del objeto enviado        
        int vsucursal       = empleado.getSucursal();
        String vrut          = empleado.getRut();
        String vprimernombre       = empleado.getPrimerNombre();
        String vsegundonombre       = empleado.getSegundoNombre();
        String vapellidopaterno      = empleado.getApellidoPaterno();
        String vapellidomaterno      = empleado.getApellidoMaterno();
        int vtelefono        = empleado.getTelefono();
        String vdireccion    = empleado.getDireccion();
        String vemail        = empleado.getEmail();
        int vtipo        = empleado.getTipo();  
        String vusername        = empleado.getUsername();
        String vpassword        = empleado.getPassword();             
            
        

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_new_empleado(?,?,?,?,?,?,?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vsucursal);
            cs.setString(3, vrut);
            cs.setString(4, vprimernombre);
            cs.setString(5, vsegundonombre);
            cs.setString(6, vapellidopaterno);
            cs.setString(7, vapellidomaterno);
            cs.setInt(8, vtelefono);
            cs.setString(9, vdireccion);
            cs.setString(10, vemail);
            cs.setInt(11, vtipo);
            cs.setString(12, vusername);
            cs.setString(13, vpassword);          
            
                        
            cs.executeQuery();
            cod_error = cs.getInt(1);           
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
        
    };
    
    
    
    // editar Empleado
    
    public int editarEmpleado(int id, Empleado empleado){
        Conector conector = new Conector();
        Connection con       = conector.connectar();
        int cod_error        = 1122; //error en procedimiento
        
        int vsucursal       = empleado.getSucursal();
        String vprimernombre       = empleado.getPrimerNombre();
        String vsegundonombre       = empleado.getSegundoNombre();
        String vapellidopaterno      = empleado.getApellidoPaterno();
        String vapellidomaterno      = empleado.getApellidoMaterno();
        int vtelefono        = empleado.getTelefono();
        String vdireccion    = empleado.getDireccion();
        String vemail        = empleado.getEmail();      
              
      

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_edit_empleado(?,?,?,?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vsucursal);
            cs.setInt(3, id);
            cs.setString(4, vprimernombre);
            cs.setString(5, vsegundonombre);
            cs.setString(6, vapellidopaterno);
            cs.setString(7, vapellidomaterno);
            cs.setInt(8, vtelefono);
            cs.setString(9, vdireccion);
            cs.setString(10, vemail);            
            
                        
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
           
    
    };
    
    
    // dar de baja Empleado
    
    public int bajaEmpleado(int id, int idSuc){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
        int vsucursalid     = idSuc;
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_cambiar_estado(?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vsucursalid);
            cs.setInt   (3, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }
    
    //Cambiar Contraseña Empleado
    
    public int cambiarContrasena(int id, int idSuc, String pass, String npass, String npass2){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
        int vsucursalid     = idSuc;
        String vpassword       = pass;
        String vpassword_nueva       = npass;
        String vpassword_nuevaa       = npass2;
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_change_password(?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vsucursalid);
            cs.setInt   (3, vid);
            cs.setString(4, vpassword);
            cs.setString(5, vpassword_nueva);
            cs.setString(6, vpassword_nuevaa);
            
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }
    
}
