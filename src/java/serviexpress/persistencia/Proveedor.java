
package serviexpress.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;

public class Proveedor {

    //Variables
private int             id;
private String          rut;
private String          nombre;
private int             telefono;
private String          direccion;
private String          email;
private int        sucursal;    


//Constructor sin parametros

    public Proveedor() {
    }
//Constructor con parametros

    public Proveedor(int id, String rut, String nombre, int telefono, String direccion, String email, int sucursal) {
        this.id = id;
        this.rut = rut;
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.email = email;
        this.sucursal = sucursal;
    }
// Mutadores y accesadores

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }
    
//Buscar    Proveedor por ID
    
    public Proveedor buscarPorID(int id){
        try {
                ArrayList<Proveedor> lista = new ArrayList<>();
                Proveedor proveedor = new Proveedor();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_proveedores_adm.sp_find_proveedor_id(?,?) }");
        cs.setInt(1, id);
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Proveedor
                proveedor.setId(rs.getInt(1));
                proveedor.setRut(rs.getString(2));
                proveedor.setNombre(rs.getString(3));
                proveedor.setTelefono(rs.getInt(4));
                proveedor.setDireccion(rs.getString(5));
                proveedor.setEmail(rs.getString(6));
                proveedor.setSucursal(rs.getInt(7));               
                
            
    } // Cierre ciclo while
           
            return proveedor;
            
            } catch (SQLException ex) {
            Logger.getLogger(Proveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };        
    
//Listar    Proveedores
    
    public ArrayList<Proveedor> listarProveedores(){
         try {
                ArrayList<Proveedor> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_proveedor_adm.sp_list_proveedores(?) }");
        cs.registerOutParameter(1, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(1);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                Proveedor objProveedor = new Proveedor();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                objProveedor.setId(rs.getInt(1));
                objProveedor.setRut(rs.getString(2));
                objProveedor.setNombre(rs.getString(3));                
                objProveedor.setTelefono(rs.getInt(4));                
                objProveedor.setDireccion(rs.getString(5));
                objProveedor.setEmail(rs.getString(6));
                objProveedor.setSucursal(rs.getInt(7));
                
                
                lista.add(objProveedor); //se carga el objeto seteado en el ciclo
            
    } // Cierre ciclo while
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(Proveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };
    
//Crear     Proveedor
    
    public int crearProveedor(Proveedor proveedor){
        Conector conector   = new Conector();
        Connection con      = conector.connectar();
        
        int cod_error       = 1122;//Codigo ERROR: error ejecucion del procedimeinto almacenado
        
        //extraccion de datos del objeto enviado
        String vnombre       = proveedor.getNombre();
        String vrut          = proveedor.getRut();
        int vtelefono        = proveedor.getTelefono();
        String vdireccion    = proveedor.getDireccion();
        String vemail        = proveedor.getEmail();
        int vsucursal       = proveedor.getSucursal();
        
        

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_proveedor_adm.sp_new_proveedor(?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setString(2, vnombre);
            cs.setString(3, vrut);
            cs.setInt   (4, vtelefono);           
            cs.setString(5, vdireccion);
            cs.setString(6, vemail);
            cs.setInt   (7, vsucursal);
            cs.executeQuery();
            cod_error = cs.getInt(1);            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Proveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
        
    };
//editar    Proveedor
    
    public int editarProveedor(int id,Proveedor proveedor){
        Conector conector = new Conector();
        Connection con       = conector.connectar();
        int cod_error        = 1122; //error en procedimiento
        
        
        int vtelefono        = proveedor.getTelefono();        
        String vdireccion    = proveedor.getDireccion();
        String vemail        = proveedor.getEmail();
        

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_proveedor_adm.sp_edit_proveedor(?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, id);            
            cs.setInt   (3, vtelefono);            
            cs.setString(4, vdireccion);
            cs.setString(5, vemail);

            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Proveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
           
    
    };        
    
//Eliminar  Proveedor
    
    public int eliminarProveedor(int id){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_proveedor_adm.sp_del_proveedor(?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Proveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }
    
    
    
//Buscar    Proveedor por ID
    
 public Proveedor buscarPorID2(int id){
             try {
                ArrayList<Proveedor> lista = new ArrayList<>();
                Proveedor proveedor = new Proveedor();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_proveedor_adm.sp_find_proveedor_id(?,?) }");
        cs.setInt(1, id);
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Condominio
                proveedor.setId(rs.getInt(1));
                proveedor.setRut(rs.getString(2));
                proveedor.setNombre(rs.getString(3));
                proveedor.setTelefono(rs.getInt(4));
                proveedor.setDireccion(rs.getString(5));
                proveedor.setEmail(rs.getString(6));
                proveedor.setSucursal(rs.getInt(7));
                

            
    } // Cierre ciclo while
           
            return proveedor;
            
            } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
     
     
 }
 

}  