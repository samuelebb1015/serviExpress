/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import serviexpress.persistencia.Pedido;

/**
 *
 * @author Samuel
 */
@WebServlet(name = "generarOrdenController", urlPatterns = {"/generarOrdenController"})
public class generarOrdenController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet generarOrdenController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet generarOrdenController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        RequestDispatcher res;
        if(request.getParameter("btnGenerar")!=null){    
        //Rescate de variables del Formulario
        
        String idProveedor = request.getParameter("proveedor");     
        int idProv =  Integer.parseInt(idProveedor);
        
        
        
        
        //Creacion de objeto para cargar datos
        Pedido objPedido = new Pedido();
        
        //Seteo de parametros a objeto
        objPedido.setIdProv(idProv);
        objPedido.setIdEmp(1);    
        
        
        
//Creamos el objeto;
int resultado = objPedido.crearOrdenPedido(objPedido);
request.setAttribute("id", objPedido.getId()); 
request.setAttribute("idProveedor", idProveedor);

            res=request.getRequestDispatcher("/generarOrden.jsp");
            res.forward(request, response);
    }
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
