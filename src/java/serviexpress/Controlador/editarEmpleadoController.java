/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import serviexpress.persistencia.Empleado;

/**
 *
 * @author Samuel
 */
@WebServlet(name = "editarEmpleadoController", urlPatterns = {"/editarEmpleadoController"})
public class editarEmpleadoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet editarEmpleadoController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet editarEmpleadoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        RequestDispatcher res;        
        if(request.getParameter("btnEditar")!=null){    
        //Rescate de variables del Formulario          
        
        int sucID = Integer.parseInt(request.getParameter("txtSuc"));
        int id = Integer.parseInt(request.getParameter("txtId"));
        String nombre1 = request.getParameter("txtNombre1");
        String nombre2 = request.getParameter("txtNombre2");
        String apellido1 = request.getParameter("txtPaterno");
        String apellido2 = request.getParameter("txtMaterno");
        int telefono = Integer.parseInt(request.getParameter("txtTelefono"));
        String direccion = request.getParameter("txtDireccion");
        String email = request.getParameter("txtEmail");        
        
        //Creacion de objeto para cargar datos
        Empleado objEmpleado = new Empleado();
        
        //Seteo de parametros a objeto
        //Cargamos el valor ID de Serviexpress
        objEmpleado.setId(id);
        objEmpleado.setSucursal(1);
        objEmpleado.setPrimerNombre(nombre1);
        objEmpleado.setSegundoNombre(nombre2);
        objEmpleado.setApellidoMaterno(apellido1);
        objEmpleado.setApellidoPaterno(apellido2);
        objEmpleado.setTelefono(telefono);
        objEmpleado.setDireccion(direccion);
        objEmpleado.setEmail(email);
        
            
        
//Creamos el objeto;
 
int resultado = objEmpleado.editarEmpleado(id, objEmpleado);
request.setAttribute("resultado", resultado);             

            res=request.getRequestDispatcher("/listarEmpleados.jsp");
            res.forward(request, response);            
    }       
         
       
    }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
    
