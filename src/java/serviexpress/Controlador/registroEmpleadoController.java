/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import serviexpress.persistencia.Cliente;
import serviexpress.persistencia.Empleado;

/**
 *
 * @author Samuel
 */
@WebServlet(name = "registroEmpleadoController", urlPatterns = {"/registroEmpleadoController"})
public class registroEmpleadoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try (PrintWriter out = response.getWriter()) {
            RequestDispatcher res;
            Empleado e = new Empleado();

            if (request.getParameter("validarEmpleado") != null) {

                //Si el usuario Existe
                if (e.loginEmpleado(request.getParameter("user"), request.getParameter("pass")) != null) {

                    //Se instancia el objeto encontrado
                    e = e.loginEmpleado(request.getParameter("user"), request.getParameter("pass"));

                    request.setAttribute("idEmpleado", e.getId());
                    request.setAttribute("user", e.getUsername());

                } else {
                    request.setAttribute("idEmpleado", "0");
                }

            }

            res = request.getRequestDispatcher("loginEmpleado.jsp");
            res.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            RequestDispatcher res;
            if (request.getParameter("btnRegistrar") != null) {
                //Rescate de variables del Formulario
                String rut = request.getParameter("txtRut");
                String nombre1 = request.getParameter("txtNombre1");
                String nombre2 = request.getParameter("txtNombre2");
                String apellido1 = request.getParameter("txtApellido1");
                String apellido2 = request.getParameter("txtApellido2");
                int telefono = Integer.parseInt(request.getParameter("txtTelefono"));
                String direccion = request.getParameter("txtDireccion");
                String email = request.getParameter("txtEmail");
                String tipo = request.getParameter("tipo");
                String username = request.getParameter("txtUser");
                String pass = request.getParameter("txtPass");

                //Creacion de objeto para cargar datos
                Empleado objEmpleado = new Empleado();

                //Seteo de parametros a objeto
                objEmpleado.setSucursal(1);//Cargamos el valor ID de Serviexpress
                objEmpleado.setRut(rut);
                objEmpleado.setPrimerNombre(nombre1);
                objEmpleado.setSegundoNombre(nombre2);
                objEmpleado.setApellidoMaterno(apellido1);
                objEmpleado.setApellidoPaterno(apellido2);
                objEmpleado.setTelefono(telefono);
                objEmpleado.setDireccion(direccion);
                objEmpleado.setEmail(email);
                if (tipo.equals("mecanico")) {
                    objEmpleado.setTipo(2);
                } else {
                    objEmpleado.setTipo(3);
                }
                objEmpleado.setUsername(username);
                objEmpleado.setPassword(pass);

//Creamos el objeto;
                String mensaje = "";
                int resultado = objEmpleado.crearEmpleado(objEmpleado);
                if (resultado == 0) {
                    mensaje = "</br>"
                            + "<div class=\"alert alert-success\">\n"
                            + "  <strong>¡Éxito!</strong> Empleado Registrado Exitosamente.\n"
                            + "</div>";
                } else {
                    mensaje = "<br>"
                            + "<div class=\"alert alert-danger\">\n"
                            + "  <strong>¡Error!</strong> Empleado no se pudo registrar.\n"
                            + "</div>";
                }
                request.setAttribute("resultado", mensaje);

                res = request.getRequestDispatcher("/registrarEmpleado.jsp");
                res.forward(request, response);

            }

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
