/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.complementos.negocio;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;

/**
 *
 * @author jhan
 */
public class DetallePedido {
    
    private int id;
    private int idOrden;
    private int idProd;
    private int cantidad;
    private int estado;
    private String fecha;

    public DetallePedido() {
    }    

    public DetallePedido(int id, int idOrden, int idProd, int cantidad, int estado, String fecha) {
        this.id = id;
        this.idOrden = idOrden;
        this.idProd = idProd;
        this.cantidad = cantidad;
        this.estado = estado;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(int idOrden) {
        this.idOrden = idOrden;
    }

    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "DetallePedido{" + "id=" + id + ", idOrden=" + idOrden + ", idProd=" + idProd + ", cantidad=" + cantidad + ", estado=" + estado + ", fecha=" + fecha + '}';
    }    
    
    
    
    public int agregarProductoDetallePedido(DetallePedido detalle){
        Conector conector   = new Conector();
        Connection con      = conector.connectar();
        
        int cod_error       = 1122;//Codigo ERROR: error ejecucion del procedimeinto almacenado
        
        //extraccion de datos del objeto enviado
        int idProducto       = detalle.getIdProd();
        int idOrd          = detalle.getIdOrden();
        int cant        = detalle.getCantidad();      
        
        

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_orden_pedido_adm.sp_add_producto(?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, idOrd);
            cs.setInt(3, idProducto);
            cs.setInt(4, cant);           
            
            cs.executeQuery();
            cod_error = cs.getInt(1);            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(DetallePedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
        
    };
    
    public DetallePedido buscarPorID(int id){
        try {
                ArrayList<DetallePedido> lista = new ArrayList<>();
                DetallePedido detPedido = new DetallePedido();
                Conector conector = new Conector();
                Connection con = conector.connectar();        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_orden_pedido_adm.sp_list_detalle_pedido(?,?) }");
        cs.setInt(1, id);      
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
        
            
            while(rs.next()){ // se ejecuta un ciclo que recorre la variable ResultSet

                // Seteo de atributos del Objeto Cliente
                detPedido.setId(rs.getInt(1));
                detPedido.setIdOrden(rs.getInt(2));                
                detPedido.setIdProd(rs.getInt(3));
                detPedido.setCantidad(rs.getInt(4));
                detPedido.setFecha(rs.getString(5));
                detPedido.setEstado(rs.getInt(6));
                                                          
            
    } // Cierre ciclo while
           
            return detPedido;
            
            } catch (SQLException ex) {
            Logger.getLogger(DetallePedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };
    
    public int confirmarPedido(int id){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
        
        
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_orden_pedido_adm.sp_confirmar_pedido(?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);           
            cs.setInt   (2, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);           
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(DetallePedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }
}
