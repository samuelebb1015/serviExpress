/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.complementos.negocio;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import serviexpress.conexion.Conector;
import serviexpress.persistencia.Empleado;


/**
 *
 * @author jhan
 */
public class TipoEmpleado {
    
    private int id;
    private String nombre;
    private String descripcion;
    private int nivel;

    public TipoEmpleado() {
    }

    public TipoEmpleado(int id, String nombre, String descripcion, int nivel) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.nivel = nivel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    @Override
    public String toString() {
        return "TipoEmpleado{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", nivel=" + nivel + '}';
    }
     
    public ArrayList<TipoEmpleado> listarTipoEmpleado(){
         try {
                ArrayList<TipoEmpleado> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_empleado_adm.sp_list_tipo_empleado(?) }");
        cs.registerOutParameter(1, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(1);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                TipoEmpleado tipoEmp = new TipoEmpleado();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                tipoEmp.setId(rs.getInt(1));
                tipoEmp.setNombre(rs.getString(2));
                tipoEmp.setDescripcion(rs.getString(3));                
                tipoEmp.setNivel(rs.getInt(4));                
                
                
                lista.add(tipoEmp); //se carga el objeto seteado en el ciclo
            
    } // Cierre ciclo while
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(TipoEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };     

    
}
