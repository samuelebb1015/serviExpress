<%-- 
    Document   : generarOrden
    Created on : 01-jul-2018, 20:10:05
    Author     : Samuel
--%>

<%@page import="serviexpress.persistencia.Producto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="serviexpress.persistencia.Proveedor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Generar Orden</h2>
        <form action="generarOrdenController" method="POST" >                  
            
            <% if(request.getAttribute("idProveedor")!=null){
            response.sendRedirect("/generarOrdenProveedor");
}%>
          Proveedor:
          <select name="proveedor">
                            <% Proveedor proveedor = new Proveedor();
                                ArrayList<Proveedor> pro = proveedor.listarProveedores();
                                if (pro != null) {
                                    for (Proveedor prov : pro) {%>
                            <option value="<%=prov.getId()%>"><%=prov.getNombre()%></option>
                            <% }
                                }%>    
                        </select>
            
                        <input type="submit" name="btnGenerar" value="Generar">
        </form>
                        
                        <%out.print("<br>");
                    out.print(request.getAttribute("id"));%>
    </body>
</html>
