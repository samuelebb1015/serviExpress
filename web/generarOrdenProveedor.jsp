<%@page import="java.util.ArrayList"%>
<%@page import="serviexpress.persistencia.Producto"%>
<%-- 
    Document   : generarOrdenProveedor
    Created on : 01-jul-2018, 20:46:29
    Author     : Samuel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>  
        <form action="generarOrdenController" method="POST" >
        <%            
            int idProveedor = 100;//Integer.parseInt(request.getParameter("idProveedor"));            
        %>
        <h1>Generar Orden para<% ;%></h1>

        Producto:
        <select name="producto">
            <% Producto producto = new Producto();
                ArrayList<Producto> pro = producto.listarProductos();
                if (pro != null) {
                    for (Producto prod : pro) {
                        if (prod.getIdProveedor() == idProveedor) {
            %>
            <option value="<%=prod.getId()%>"><%=prod.getNombre()%></option>
            <% }
                    }
                }%>    
        </select>
        <br />      
        </form>  
              
    </body>
</html>
