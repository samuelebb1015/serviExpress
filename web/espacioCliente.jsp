<%-- 
    Document   : espacioCliente
    Created on : 26-05-2018, 15:26:34
    Author     : jhan - Lefutray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <title>Dashboard Cliente</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        <div class="row">
            <% HttpSession ses = request.getSession();

                if (ses.getAttribute("sesion") != null) {

                    out.print("Bienvenid@ " + ses.getAttribute("sesion"));

                    //if ((Integer) ses.getAttribute("idC") == 1) {
                        //out.print("<br> <h1> eres administrador </h1>");
                    //}

                    //if ((Integer) ses.getAttribute("idC") == 2) {
                        //out.print("<br> <h1> eres usuario </h1>");
                    //}
                } else {
                    response.sendRedirect("loginClient.jsp");
                }

                if (request.getParameter("c") != null) {
                    ses.invalidate();
                    response.sendRedirect("loginClient.jsp");
                }
            %>
        </div>
    </div>
<%@include file="Template/footer.jsp"%>
</body>
</html>
