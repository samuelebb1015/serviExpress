<%-- 
    Document   : loginClient
    Created on : 26-05-2018, 15:25:35
    Author     : Lefutray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>

    <%@include file="Template/head.jsp"%>
    <title>Acceso Cliente</title>
    <link href="Assets/css/signin.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <ol class="breadcrumb">
        <li><a href=index.jsp>Inicio</a></li>
        <li class="active">Acceso Cliente</li>
    </ol>
    <div class="container">
        <form class="form-signin" action="validarClienteController" method="POST" >
            <div align="center">
                <h3 class="form-signin-heading">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    Acceso Cliente
                </h3>
            </div>
            <div class="form-group">
            <label class="sr-only">Usuario</label>
            <input type="text" name="user" class="form-control" placeholder="Usuario" required="" autofocus="">
            </div>
            <div class="form-group">
            <label class="sr-only">Contraseña</label>
            <input type="password" name="pass" class="form-control" placeholder="Contraseña" required="">
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block" name="validarClient" value="Iniciar Sesión">
        </form>
        <center>
            <a href=autoRegistrarCliente.jsp>Registrarse</a>
        </center>
    </div>

<%
    if (request.getAttribute("idCliente") != null) {
        if (request.getAttribute("idCliente") != "0") {
            HttpSession ses = request.getSession();
            ses.setAttribute("sesion", request.getAttribute("user"));
            ses.setAttribute("idC", request.getAttribute("idCliente"));

            response.sendRedirect("espacioCliente.jsp");
        } else {
            out.print("<br><h3>Credenciales incorrectas</h3>");
        }
    }
%>
<%@include file="Template/footer.jsp"%>
</body>
</html>
