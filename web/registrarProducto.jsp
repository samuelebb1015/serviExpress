<%-- 
    Document   : registrarProducto
    Created on : 27-jun-2018, 2:17:08
    Author     : Samuel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<%@page import="serviexpress.complementos.negocio.*" %>

<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registro de Producto</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        <div class="page-header">
            <button class="btn btn-default pull-right" onclick="goBack()">Volver</button>
            <h3>Registro de Producto</h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="registroProductoController" method="POST" >
                <div class="col-md-4">
                    <div class="form-group">
                        <label>
                            Nombre:
                        </label>
                        <input class="form-control" type="text" name="txtNombre" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            Descripción:
                        </label>
                        <textarea class="form-control" rows="7" name="txtDescripcion" rows="10" cols="40"></textarea>
                    </div>
                    <div class="form-group">
                        <label>
                            Precio Venta :
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                            <input class="form-control" type="text" name="txtPrecio">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>
                            Stock:
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-plus-sign"></i></span>
                            <input class="form-control" type="text" name="txtStock">
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label>
                            Stock Crítico
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-exclamation-sign"></i></span>
                        <input class="form-control" type="text" name="txtCritico">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            Categoría:
                        </label>
                        <select class="form-control" name="categoria">
                            <% CategoriaProducto categoria = new CategoriaProducto();
                                ArrayList<CategoriaProducto> cat = categoria.listarCategorias();
                                if (cat != null) {
                                    for (CategoriaProducto cats : cat) {%>
                            <option value="<%=cats.getId()%>"><%=cats.getNombre()%></option>
                            <% }
                                } %>                  
                        </select>
                    </div>
                    <div class="form-group">
                        <label>
                            Proveedor:
                        </label>
                        <select class="form-control" name="proveedor">
                            <% Proveedor proveedor = new Proveedor();
                                ArrayList<Proveedor> pro = proveedor.listarProveedores();
                                if (pro != null) {
                                    for (Proveedor prov : pro) {%>
                            <option value="<%=prov.getId()%>"><%=prov.getNombre()%></option>
                            <% }
                                }%>    
                        </select>
                    </div>

                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="btnRegistrar" value="Registrar">
                    </div>

            </form>

            <%
                //out.print("<br>");
                //out.print(request.getAttribute("resultado"));

                //if (request.getAttribute("resultado") == "0") {
                //  out.print("<br><h2> Registrado Exitosamente</h2>");
                //}
                //if (request.getAttribute("resultado") == "1") {
                //  out.print("<br><h2>ERROR : Cliente No se pudo registrar");
                //}
            %>
        </div>
    </div>
</div>

<%@include file="Template/footer.jsp"%>
</body>
</html>
