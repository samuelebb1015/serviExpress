<%@page import="java.util.ArrayList"%>
<%@page import="serviexpress.complementos.negocio.TipoEmpleado"%>
<%@page import="serviexpress.persistencia.Empleado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Editar - ServiExpress</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        <div class="page-header">
            <a class="btn btn-default pull-right" type="button" href="listarEmpleados.jsp" name="btnVolver">Volver</a>
            <h3>Edición Empleado</h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
                <form action="editarEmpleadoController" method="POST" >
                    <div class="col-md-4">
                    <%                        
                    int codigo = Integer.parseInt(request.getParameter("idEmp"));
                        String t = "";
                        Empleado empleado = new Empleado();
                        empleado = empleado.buscarPorID(codigo, 1);

                        if (empleado.getTipo() == 1) {
                            t = "Administrador";
                        }
                        if (empleado.getTipo() == 2) {
                            t = "Mecánico";
                        }
                        if (empleado.getTipo() == 3) {
                            t = "Cajero";
                        };
                    %>  
                    <div class="form-group">
                        <label>Rut: </label>
                        <input class="form-control" disabled  type="text" value="<%= empleado.getRut()%>" />
                    </div>
                    <input type="hidden" name="txtId" value="<%=empleado.getId()%>"/>
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input class="form-control" type="text" name="txtNombre1" value="<%=empleado.getPrimerNombre()%>" required/>
                    </div>
                    <div class="form-group">
                        <label>Segundo nombre:</label>
                        <input class="form-control" type="text" name="txtNombre2" value="<%=empleado.getSegundoNombre()%>" required/>
                    </div>
                    <div class="form-group">
                        <label>Apellido paterno:</label>
                        <input class="form-control" type="text" name="txtPaterno" value="<%=empleado.getApellidoPaterno()%>" required>
                    </div>
                    <div class="form-group">
                        <label>Apellido materno:</label>
                        <input class="form-control" type="text" name="txtMaterno" value="<%=empleado.getApellidoMaterno()%>" required>
                    </div>
                    <div class="form-group">
                        <label>Dirección:</label>
                        <input class="form-control" type="text" name="txtDireccion" value="<%=empleado.getDireccion()%>" required>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                        <label>Email:</label>
                        <input class="form-control" type="text" name="txtEmail" value="<%=empleado.getEmail()%>" required="">
                    </div>
                    <div class="form-group">
                        <label>Teléfono:</label>
                        <input class="form-control" type="text" name="txtTelefono" value="<%=empleado.getTelefono()%>" required>
                    </div>
                    <div class="form-group">
                        <label>ID Sucursal:</label>
                        <input class="form-control" type="text" name="txtSuc" value="<%=empleado.getSucursal()%>"/>
                    </div>
                    <div class="form-group">
                        <label>Usuario: </label>
                        <input type="text" disabled class="form-control" value="<%= empleado.getUsername()%>" />
                    </div>
                    <div class="form-group">
                        <label>Rol Empleado: </label>
                        <input value="<%= empleado.getTipo()%>" type="hidden" />
                        <input type="text" class="form-control" disabled value="<%= t%>" />
                    </div>
                    <br>
                    <div class="form-group">
                    <input class="btn btn-success" type="submit" name="btnEditar" value="Guardar">
                    </div>
                </form>
                    
                <%
                    //out.print("<br>");
                    //out.print(request.getAttribute("resultado"));

                    //if (request.getAttribute("resultado") == "0") {
                    //    out.print("<br><h2>Cliente Editado Exitosamente</h2>");
                    //}

                    //if (request.getAttribute("resultado") == "1") {
                    //    out.print("<br><h2>ERROR : Empleado No se pudo Editar");
                    //}
                %>
            </div>
        </div>
    </div>

    <%@include file="Template/footer.jsp"%>
</body>

</html>
