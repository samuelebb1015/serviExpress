<%-- 
    Document   : listarClientes
    Created on : 28-may-2018, 1:33:58
    Author     : Samuel - Lefutray
--%>
<%@page import="serviexpress.complementos.negocio.TipoEmpleado"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<!DOCTYPE html>
<html lang="es-ES">
    <%@include file="Template/head.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ServiExpress - Empleados</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        <div class="page-header">
            <h3> Listado de Empleados</h3>
        </div>
        <div class="row">
            <table id="example" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>RUT</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Dirección</th>
                        <th>Email</th>
                        <th>Teléfono</th>
                        <th>Sucursal</th>
                        <th>Usuario</th>
                        <th>Rol</th> 
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <%                    String t = "";
                        Empleado empleado = new Empleado();
                        //TipoEmpleado tipo = new TipoEmpleado();
                        ArrayList<Empleado> lista = empleado.listarEmpleados(1);
                        //ArrayList<TipoEmpleado> list = tipo.listarTipoEmpleado();
                        for (Empleado e : lista) {
                            if (e.getTipo() == 1) {
                                t = "Administrador";
                            }
                            if (e.getTipo() == 2) {
                                t = "Mecánico";
                            }
                            if (e.getTipo() == 3) {
                                t = "Cajero";
                            }
                    %>

                    <tr>
                        <td><%=e.getRut()%> </td>
                        <td><%=e.getPrimerNombre()%> </td>
                        <td><%=e.getApellidoPaterno()%> </td>
                        <td><%=e.getDireccion()%> </td>
                        <td><%=e.getEmail()%> </td>
                        <td><%=e.getTelefono()%> </td>
                        <td><%= "ServiExpress"%> </td>
                        <td><%= e.getUsername()%> </td>
                        <td><%= t%> </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Acciones <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="editarEmpleado.jsp?idEmp=<%=e.getId()%>"><span class="glyphicon glyphicon-edit"></span>&nbsp;Editar</a></li>
                                    <li><a href="listarEmpleados.jsp?idEmp=<%=e.getId()%>"><span class="glyphicon glyphicon-trash"></span>&nbsp;Eliminar</a></li>
                                </ul>

                            </div>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
        </div>
    </div>
    <%  if (request.getParameter("idEmp") != null) {
            Empleado emp = new Empleado();
            int cod = Integer.parseInt(request.getParameter("idEmp"));
            emp.bajaEmpleado(cod, 1);
            request.getRequestDispatcher("listarEmpleados.jsp");
        }
    %>
    <%@include file="Template/footer.jsp"%>
</body>


</html>
