<%-- 
    Document   : registrar
    Created on : 27-may-2018, 0:51:32
    Author     : Lefutray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <title>Registro de Empleado</title>
    <body>
        <ol class="breadcrumb">
            <li><a href=index.jsp>Inicio</a></li>
            <li><a href=loginEmpleado.jsp>Acceso Empleado</a></li>
            <li class="active">Registro Empleado</li>
        </ol>
        <div class="container">
            <div class="row">
                <div class="page-header">
                    <h1>Registro de Empleado</h1>
                </div>
            </div>
            <div class="row">
                <form action="registroEmpleadoController" method="POST" >
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nombre Usuario</label>
                            <input name="txtUser" class="form-control" type="text" placeholder="Usuario" required/>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input name="txtPass" class="form-control" type="password" placeholder="*******" required/>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input name="txtNombre1" class="form-control" type="text" placeholder="Nombre" required/>
                        </div>
                        <div class="form-group">
                            <label>Segundo nombre</label>
                            <input name="txtNombre2" class="form-control" type="text" placeholder="Segundo nombre"/>
                        </div>
                        <div class="form-group">
                            <label>Primer Apellido</label>
                            <input name="txtApellido1" class="form-control" type="text" placeholder="Apellido Paterno" required/>
                        </div>
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                            <input name="txtApellido2" class="form-control" type="text" placeholder="Apellido Materno" required/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>RUT</label>
                            <input name="txtRut" maxlength="12" class="form-control" type="text" placeholder="11111111-0" required/>
                        </div>
                        <div class="form-group">
                            <label>Telefono</label>
                            <input name="txtTelefono" class="form-control" type="text" placeholder="955566777" required/>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input name="txtEmail" class="form-control" type="text" placeholder="usuario@mail.com" required/>
                        </div>
                        <div class="form-group">
                            <label>Dirección</label>
                            <input name="txtDireccion" class="form-control" type="text" placeholder="Calle, #123, Ciudad, Region" required/>
                        </div>
                        <div class="form-group">
                            <label>Tipo Empleado</label>
                            <select name="tipo" required class="form-control" >
                                <option value="">Seleccione Tipo...</option>
                                <option value="mecanico">Mecánico</option>
                                <option value="cajero">Cajero</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="btnRegistrar" class="btn btn-primary pull-right" value="Registrar">
                        </div>
                </form>
            </div>
            <div class="col-md-4">
                <%
                    if (request.getAttribute("resultado")!=null) {
                        out.print(request.getAttribute("resultado"));
                    }else{
                        //out.print(request.getAttribute("resultado")+"else");
                    }
                %>
            </div>
        </div>
    </div>
    <%@include file="Template/footer.jsp"%> 
</body>
</html>
