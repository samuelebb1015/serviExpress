<%@page import="serviexpress.persistencia.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="serviexpress.complementos.negocio.TipoEmpleado"%>
<%@page import="serviexpress.persistencia.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Edición de Cliente</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        
        <div class="page-header">
            <button class="btn btn-default pull-right" onclick="goBack()">Volver</button>
            <h3>Edición Cliente</h3>
            
        </div>
        
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form action="editarClienteController" method="POST" >             

                    <%                    int codigo = 0;

                        if (request.getParameter("idCli2") != null) {
                            codigo = Integer.parseInt(request.getParameter("idCli2"));
                        }

                        String t = "Activo";
                        Cliente cliente = new Cliente();

                        if (request.getParameter("fiadoId") != null) {
                            int codii = Integer.parseInt(request.getParameter("fiadoId"));
                            cliente.editarFiado(codii, 1);
                            codigo = Integer.parseInt(request.getParameter("fiadoId"));
                        }

                        if (request.getParameter("estadoId") != null) {
                            int estado = Integer.parseInt(request.getParameter("estadoId"));
                            cliente.bajaCliente(estado, 1);
                            codigo = Integer.parseInt(request.getParameter("estadoId"));
                        }

                        cliente = cliente.buscarPorID(codigo, 1);

                        if (cliente.getEstado() == 0) {
                            t = "Inactivo";
                        }


                    %>                   
                    
                        <input class="form-control" type="hidden" name="txtId" value="<%=cliente.getId()%>" />
                    
                    <div class="form-group">
                        <label>Rut:</label>
                        <input class="form-control" disabled value="<%= cliente.getRut()%>"/>
                    </div>
                    <div class="form-group">
                        <label>Nombre de Usuario:</label>
                        <input class="form-control" disabled value="<%= cliente.getUsername()%>"/>
                    </div>
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input class="form-control" type="text" name="txtNombre1" value="<%=cliente.getPrimerNombre()%>" required/>
                    </div>
                    <div class="form-group">
                        <label>Segundo nombre:</label>
                        <input class="form-control" type="text" name="txtNombre2" value="<%=cliente.getSegundoNombre()%>" required/>
                    </div>
                    <div class="form-group">
                        <label>Apellido Paterno:</label>
                        <input class="form-control" type="text" name="txtPaterno" value="<%=cliente.getApellidoPaterno()%>" required>
                    </div>
                    <div class="form-group">
                        <label>Apellido Materno:</label>
                        <input class="form-control" type="text" name="txtMaterno" value="<%=cliente.getApellidoMaterno()%>" required>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                        <label>Dirección:</label>
                        <input class="form-control" type="text" name="txtDireccion" value="<%=cliente.getDireccion()%>" required>
                    </div>
                    <div class="form-group">
                        <label>Email:</label>
                        <input class="form-control" type="text" name="txtEmail" value="<%=cliente.getEmail()%>" required="">
                    </div>
                    <div class="form-group">
                        <label>Telefono:</label>
                        <input class="form-control" type="text" name="txtTelefono" value="<%=cliente.getTelefono()%>" required>
                    </div>
                    <div class="form-group">
                        <label>ID Sucursal:</label>
                        <input class="form-control" type="text" name="txtSuc" value="<%=cliente.getSucursal()%>" required/>
                    </div>
                    
                    <%  if (cliente.getEstado() == 1) {%>
                    <div class="form-group">
                        <a class="btn btn-warning" href="editarClientes.jsp?estadoId=<%=cliente.getId()%>">Dar de Baja</a>
                        </div>
                    <% } else {%>
                    <div class="form-group">
                        <a class="btn btn-warning" href="editarClientes.jsp?estadoId=<%=cliente.getId()%>">Reincorporar</a>
                        </div>
                    <% } %>
                    <%  if (cliente.getFiado() == 1) {%>
                    <div class="form-group">
                        <a class="btn btn-danger" href="editarClientes.jsp?fiadoId=<%=cliente.getId()%>">Desactivar</a>
                        </div>
                    <% } else {%>
                    <div class="form-group">
                        <a class="btn btn-primary" href="editarClientes.jsp?fiadoId=<%=cliente.getId()%>">Activar</a>
                    </div>
                    <% } %>
                    <%
                            //request.getRequestDispatcher("listarClientes.jsp");
                    %>
                    
                        <input class="btn btn-success" type="submit" name="btnEditar" value="Guardar">
                        
                    </div>

                </form>

                <% //            out.print("<br>");
//                    out.print(request.getAttribute("resultado"));

//                    if (request.getAttribute("resultado") == "0") {
//                        out.print("<br><h2>Cliente Editado Exitosamente</h2>");
//                    }

//                    if (request.getAttribute("resultado") == "1") {
//                        out.print("<br><h2>ERROR : Empleado No se pudo Editar");
//                    }
                %>
            </div>
        </div>
    </div>
    <%@include file="Template/footer.jsp"%>
</body>

</html>