<%-- 
    Document   : index
    Created on : 27-may-2018, 15:08:33
    Author     : Lefutray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <title>Inicio</title>
</head>
<body>
    <%@include file="Template/header.jsp"%>
    <div class="container">
        <div class="row">
            <div class="page-header">
                <h4>ServiExpress - Sistema de Gestión de Talleres Mecánicos</h4>
            </div>
        </div>
    </div>
    <%@include file="Template/footer.jsp"%>
</body>
</html>
