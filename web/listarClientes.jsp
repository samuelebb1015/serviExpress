<%-- 
    Document   : listarClientes
    Created on : 28-may-2018, 1:33:58
    Author     : Lefutray
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ServiExpress - Listar Clientes</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        <div class="page-header">
            <h3> Listado de Clientes</h3>
        </div>
        <div class="row">


            <table id="example" class="table table-striped table-hover" >
                <thead>
            <tr>
                <th>Rut</th>
                <th>Primer Nombre</th>
                <th>Apellido Paterno</th>
                <th>Dirección</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Username</th>
                <th>Fiado</th> 
                <th>Opción</th>
            </tr>
             </thead>
                <tbody>
            <%
                String fiado = "";
                Cliente cliente = new Cliente();
                ArrayList<Cliente> lista = cliente.listarClientes(1);
                for(Cliente c: lista){
               
                if(c.getFiado()== 1){
                    fiado = "Si";
                }else{
                    fiado = "No";
                }
                %>
        
                <tr>
                    <td><%=c.getRut()%> </td>
                    <td><%=c.getPrimerNombre()%> </td>
                    <td><%=c.getApellidoPaterno()%> </td>
                    <td><%=c.getDireccion() %> </td>
                    <td><%=c.getEmail()%> </td>
                    <td><%=c.getTelefono()%> </td>
                    <td><%=c.getUsername()%> </td>
                    <td><%= fiado %> </td>
                    <td><a class="btn btn-primary btn-sm"  href="editarClientes.jsp?idCli2=<%=c.getId()%>"><span class="glyphicon glyphicon-edit"></span>&nbsp;Editar</a></td>
                </tr>
        <% 
        }
        %>
          </tbody>
            </table>
        </div>
    </div>
    <%@include file="Template/footer.jsp"%>
    </body>
</html>
