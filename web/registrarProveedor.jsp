
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registro de Proveedor</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        <div class="page-header">
            <button class="btn btn-default pull-right" onclick="goBack()">Volver</button>
            <h3>Registro de Proveedor</h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="registroProveedor" method="POST" >
                <div class="col-md-4">  
                    <div class="form-group">
                        <label>
                            Rut:
                        </label>
                        <input class="form-control" type="text" name="txtRut" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            Nombre:
                        </label>
                        <input class="form-control" type="text" name="txtNombre1" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            Telefono:
                        </label>
                        <input class="form-control" type="text" name="txtTelefono" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            Dirección:
                        </label>
                        <input class="form-control" type="text" name="txtDireccion" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            Email:
                        </label>
                        <input class="form-control" type="text" name="txtEmail" required/>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="btnRegistrar" value="Registrar">
                    </div>
            </form>
        </div>

        <%//                       out.print("<br>");
            //out.print(request.getAttribute("resultado"));
            //if (request.getAttribute("resultado") == "0") {
            //  out.print("<br><h2>Cliente Registrado Exitosamente</h2>");
            //}
            //if (request.getAttribute("resultado") == "1") {
            //  out.print("<br><h2>ERROR : Cliente No se pudo registrar");
            // }

        %>
    </div>
</div>
<%@include file="Template/footer.jsp"%>
</body>
</html>
