<head>
    <link rel="icon" type="image/ico" href="Assets/images/favicon.ico"/>
    <link href="Assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="Assets/css/sticky-footer.css" rel="stylesheet" type="text/css"/>
    <link href="Assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="Assets/css/bootstrap-multiselect.css" type="text/css"/>

    <script src="Assets/js/jquery-3.3.1.js" type="text/javascript"></script>
    <script src="Assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="Assets/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="Assets/js/bootstrap-multiselect.js"></script>
    <script src="Assets/js/bootstrap.js" type="text/javascript"></script>
    <script>
    $(document).ready(function () {
        $('#example').DataTable({
            "language":
                    {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ning�n dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "�ltimo",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
        });
    });
</script>