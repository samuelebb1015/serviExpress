<nav class="navbar navbar-inverse">

    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <center>
                <a class="navbar-brand" href="#">
                    <img src="Assets/images/logo.png" alt="Brand" width="50%" />
                </a>
            </center>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <%
                    HttpSession bdas = request.getSession();
                    if (bdas.getAttribute("sesion") != null) {
                        out.print("<li><a href=espacioCliente.jsp>Dashboard</a></li>");
                        out.print("<li><a href=reservaHora.jsp>Reservar Hora</a></li>");
                        out.print("<li><a href=faqCliente.jsp>Preguntas frecuentes</a></li>");
                    } else {
                        out.print("<li><a href=index.jsp>Inicio <span class='sr-only'>(current)</span></a></li>"
                                + "<li><a href='#'>Sobre nosotros</a></li>"
                                + "<li><a href='#'>Contacto</a></li>");
                    }
                %>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <%
                    HttpSession log = request.getSession();
                    if (log.getAttribute("sesion") != null) {
                        out.print("<li><a href=espacioCliente.jsp?c=1><span class='glyphicon glyphicon-log-in'></span> Salir</a></li>");
                    } else {
                        out.print("<li><a href=loginClient.jsp ><span class='glyphicon glyphicon-log-in'></span> Acceder</a></li>");
                    }
                %>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>