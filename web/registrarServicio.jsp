<%-- 
    Document   : registrarServicio
    Created on : 20-jun-2018, 22:51:28
    Author     : Samuel - Lefutray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="Template/head.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registro de Servicio</title>
</head>
<body>
    <%@include file="Template/header.jsp" %>
    <div class="container">
        <div class="page-header">
            <button class="btn btn-default pull-right" onclick="goBack()">Volver</button>
            <h3>Registro de Servicio</h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="registroServicioController" method="POST" >
                <div class="col-md-4">
                    <div class="form-group">
                        <label>
                            Nombre servicio:
                        </label>
                        <input class="form-control" type="text" name="txtNombre" required/>
                    </div>
                    <div class="form-group">
                        <label>
                            Descripción:
                        </label>
                        <textarea class="form-control" name="txtDescripcion" rows="6" cols="40"></textarea>
                    </div>
                    <div class="form-group">
                        <label>
                            Valor servicio: 
                        </label>
                        <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                        <input class="form-control" type="text" name="txtPrecio">
                        </div>
                    </div>
                    <div class="form-group">
                        
                        <input class="btn btn-primary" type="submit" name="btnRegistrar" value="Registrar">
                    </div>

            </form>
        </div>
    </div>
</div>
<%@include file="Template/footer.jsp"%>
</body>
</html>
